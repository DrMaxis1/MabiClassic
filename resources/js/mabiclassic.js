(function ($) {
    "use Strict";
    /*----------------------------
    1. preloader Activation
    -----------------------------*/
    $(window).on('load', function () {
        $(".preloader").fadeOut("slow");
    });
    
    /*----------------------------
    2. Mobile Menu Activation
    -----------------------------*/
    $('.mobile-menu nav').meanmenu({
        meanScreenWidth: "991",
    });
    

    /*----------------------------
    3. ScrollUp Activation
    -----------------------------*/
    $.scrollUp({
        scrollName: 'scrollUp', // Element ID
        topDistance: '300', // Distance from top before showing element (px)
        topSpeed: 1000, // Speed back to top (ms)
        animation: 'fade', // Fade, slide, none
        scrollSpeed: 900,
        animationInSpeed: 1000, // Animation in speed (ms)
        animationOutSpeed: 1000, // Animation out speed (ms)
        scrollText: '<i class="fa fa-angle-up" aria-hidden="true"></i>', // Text for element
        activeOverlay: false // Set CSS color to display scrollUp active point, e.g '#00FFFF'
    });

})(jQuery);
