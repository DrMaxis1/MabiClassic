<!--/-/-/-/-/-/-/-/-/ 
Start Right Side Content  
-/-/-/-/-/-/-/-/-/-->

<div class="col-md-3 hidden-sm hidden-xs">

    <div class="panel panel-dark pd-15 " >
        <h4 style="border-bottom: 1px solid #aaa; padding-bottom: 5px;">Current Time</h4>

        <div class="table-responsive">
            <table class="pdtable">
                <tbody>
                    <tr>
                        <th>Erinn Time</th>
                        <th>Local</th>
                        <th>Server</th>

                    </tr>


                    <tr>
                        <td data-th="Erinn Time">12:23</td>
                        <td data-th="Local" class="success">01:24</td>
                        <td data-th="Server" class="success">2:33</td>

                    </tr>

                </tbody>
            </table>

        </div>
        <p class="text-center">The day is  <b> <i>Lughnasadh<i></b></p>
    </div>

    <div class="panel panel-dark pd-15 ">
        <h4 style="border-bottom: 1px solid #aaa; padding-bottom: 5px;">Moongate Times</h4>

        <div class="table-responsive">
            <table class="pdtable">
                <tbody>
                    <tr>
                        <th>Time</th>
                        <th>Area</th>
                    </tr>

                    <tr>
                        <td data-th="Time">12:23</td>
                        <td data-th="Area" class="success">Tara</td>


                    </tr>
                    <tr>
                        <td data-th="Time">12:23</td>
                        <td data-th="Area" class="success">Taillteann</td>


                    </tr>
                    <tr>
                        <td data-th="Time">17:23</td>
                        <td data-th="Area" class="success">Dunbarton</td>


                    </tr>
                    <tr>
                        <td data-th="Time">02:23</td>
                        <td data-th="Area" class="success">Port Ceann</td>


                    </tr>
                    <tr>
                        <td data-th="Time">14:23</td>
                        <td data-th="Area" class="success">Bangor</td>


                    </tr>


                </tbody>
            </table>

        </div>
        <p class="text-center">Ceo in <b> 10</b> Days</p>
    </div>


    <div class="panel panel-dark pd-15 ">
        <h4 style="border-bottom: 1px solid #aaa; padding-bottom: 5px;">Price Location</h4>

        <div class="table-responsive">
            <table class="pdtable">
                <tbody>
                    <tr>
                        <th>Time</th>
                        <th>Area</th>


                    </tr>


                    <tr>
                        <td data-th="Time">12:23</td>
                        <td data-th="Area">Dugald Aisle Logging Camp Hut</td>


                    </tr>

                    <tr>
                        <td data-th="Time">18:33</td>
                        <td data-th="Area">Outside Tir Chonaill Inn</td>


                    </tr>

                </tbody>
            </table>

        </div>

    </div>
</div>