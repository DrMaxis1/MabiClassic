<!--/-/-/-/-/-/-/-/-/
Begin Product ShowCase 
-/-/-/-/-/-/-/-/-/-/-/-->
<div class="col-xs-12 col-md-6">
   
        <div class="col-xs-12">
                <div class="section-title text-center mb-40">
                    <span class="section-desc mb-15" style="color:black;">Top news during this week</span>
                    <h3 class="section-info">News & Announcements</h3>
                </div>
            </div>

                <div class="panel panel-dark pd-30">
                    <div class="main-img">
                        <img src="" class="active" alt="product-image" id="primaryImg" />
                    </div>

                    <div class="center-content">
                        <p>
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odio deserunt dolor tenetur aliquid! Vitae quis blanditiis omnis
                            inventore placeat, dolore, earum, culpa aspernatur dignissimos ex laudantium perspiciatis repellat
                            ea mollitia?
                        </p>
                    </div>
                </div>




            <!--/-/-/-/-/-/-/-/-/ 
                    End Row 
                -/-/-/-/-/-/-/-/-/-/-->


       
        <!--/-/-/-/-/-/-/-/-/ 
                End Container 
            -/-/-/-/-/-/-/-/-/-/-->
  
    <!--/-/-/-/-/-/-/-/-/ 
            End Product ShowCase
        -/-/-/-/-/-/-/-/-/-/-->

</div>