<!--/-/-/-/-/-/-/-/-/ 
Start Left Side Content  
-/-/-/-/-/-/-/-/-/-->

<div class="col-md-3">
    <div class="panel panel-dark pd-15">
        <h4 style="border-bottom: 1px solid #aaa; padding-bottom: 5px;">Welcome to MabiClassic</h4>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Aut similique repellendus optio, asperiores excepturi debitis
            magnam eum repudiandae velit, ex repellat ducimus quos error porro reiciendis hic? Quam, libero qui. </p>
    </div>
    <div class="panel panel-dark pd-15 ">
        <h4 style="border-bottom: 1px solid #aaa; padding-bottom: 5px;">Server Status<small style="font-size: 70%" title="Not in realtime, yet"> Updated every minute</small></h4>

        <div class="table-responsive">
            <table class="pdtable">
                <tbody>
                    <tr>
                        <th>Channel</th>
                        <th>Status</th>

                    </tr>


                    <tr>
                        <td data-th="Channel">Ch1</td>
                        <td data-th="Status" class="has-success">Online
                        </td>

                    </tr>
                    <tr>
                        <td data-th="Channel">Ch2</td>
                        <td data-th="Status" class="has-warning">Online
                        </td>

                    </tr>
                    <tr>
                        <td data-th="Channel">Ch3</td>
                        <td data-th="Status" class="has-error">Online
                        </td>

                    </tr>

                </tbody>
            </table>

        </div>
        <p class="text-center"><b>185</b> players active in the last 24 hours</p>
    </div>


    <div class="panel panel-dark pd-15 ">
        <h4 style="border-bottom: 1px solid #aaa; padding-bottom: 5px;">Server Rates</h4>

        <div class="table-responsive">
            <table class="pdtable">
                <tbody>
                    <tr>
                        <th>Attribute</th>
                        <th>Rate</th>

                    </tr>


                    <tr>
                        <td data-th="Attribute">EXP</td>
                        <td data-th="Rate" class="success">2x
                        </td>

                    </tr>
                    <tr>
                        <td data-th="Attribute">EXP</td>
                        <td data-th="Rate" class="success">2x
                        </td>

                    </tr>
                    <tr>
                        <td data-th="Attribute">EXP</td>
                        <td data-th="Rate" class="success">2x
                        </td>

                    </tr>
                    <tr>
                        <td data-th="Attribute">EXP</td>
                        <td data-th="Rate" class="success">2x
                        </td>

                    </tr>
                    <tr>
                        <td data-th="Attribute">EXP</td>
                        <td data-th="Rate" class="success">2x
                        </td>

                    </tr>
                    <tr>
                        <td data-th="Attribute">EXP</td>
                        <td data-th="Rate" class="success">2x
                        </td>

                    </tr>


                </tbody>
            </table>

        </div>

    </div>
</div>