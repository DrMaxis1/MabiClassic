@extends('layouts.ui') 
@section('canonical-url') {{URL::current()}}
@endsection
 
@section('xcss')
<style>
    .fa-heart {
        color: #e74c3c;
    }

    [class^="fa fa-star"] {
        color: #f1c40f;
    }

    .fa-quote-right {
        font-size: .5em;
    }

    section.panel-title {
        padding: 15px;
        padding-top: 0;
    }

    #user-description {
        height: 100%;
        border-left: 2px solid #444;
        margin: 0 auto;
        text-align: center;
    }

    figure img {
        display: inline !important;
    }

    h4.online:before {
        background-color: green;
        height: 10px;
        width: 10px;
        border: 2px solid #11f464;
    }

    dt {
        text-align: left !important;
        width: 37% !important;
    }

    dd {
        margin-left: 2% !important;
    }

    .panel-footer {
        width: 100%;
        min-height: 40px;
    }
</style>
@endsection
 
@section('content')

<section class="container">
    <section class="row clearfix">
        <section class="col-md-12 column">

            <ol class="breadcrumb">
                <li><a href="{{route('fourms.index')}}">Fourms</a></li>
                <li><a href="{{route('fourms.show', $board->slug)}}">{{$board->slug}}</a></li>
                <li class="active">{{$thread->title}}</li>
            </ol>
        </section>
    </section>
    <section class="row clearfix">
        <section class="col-md-12 column">

            <nav class="navbar navbar-default" role="navigation">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
                        <a class="navbar-brand" href="#">MabiClassic Fourms</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="#"><i class="fa fa-comments"></i> Similar posts</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i> Tools <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#"><i class="fa fa-send"></i> Email this page</a></li>
                                    <li><a href="#"><i class="fa fa-print"></i> Print this page</a></li>
                                    <li><a href="#"><i class="fa fa-download"></i> Download pdf of this page</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#"><i class="fa fa-smile-o"></i> Subscript me in this post</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#"><i class="fa fa-cogs"></i> Manage this post(for adminstrator)</a></li>
                                </ul>
                            </li>
                        </ul>
                        <form class="navbar-form navbar-left" role="search">
                            <div class="form-group">
                                <input class="form-control" placeholder="Search in this post..." type="text">
                            </div>
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </form>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#"><i class="fa fa-heart"> </i> 17 Likes</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-share"></i> Share It! <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#"><i class="fa fa-facebook"></i> facebook</a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i> twitter</a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i> google pluse</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#"><i class="fa fa-cogs"></i> Manage topic(for adminstrator)</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </nav>

            <div class="row clearfix">
                <div class="col-md-12 column">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <section class="panel-title">
                                <time class="pull-right">
                          <i class="fa fa-calendar"></i> 2006-09-15 , <i class="fa fa-clock-o"></i> 1:35 pm
                          </time>
                                <section class="pull-left" id="id">
                                    <abbr title="count of posts in this topic">#1</abbr>
                                </section>
                            </section>
                        </div>
                        <section class="row panel-body">
                            <section class="col-md-9">
                                <h2> <i class="fa fa-smile-o"></i>{{$thread->title}}</h2>
                                <hr>
                                
                               {{$thread->subject_text}} 
                            </section>

                            <section id="user-description" class="col-md-3 ">
                                <section class="well">
                                    <div class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cricle"></i>Invalidname<span class="caret"></span></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#"><i class="fa fa-user"></i> See profile</a></li>
                                            <li><a href="#"><i class="fa fa-envelope"></i> Send PM</a></li>
                                            <li><a href="#"><i class="fa fa-code"></i>View all Articles</a></li>
                                            <li><a href="#"><i class="fa fa-th-list"></i>View all Posts</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#"><i class="fa fa-plus"></i>Add to contact List</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#"><i class="fa fa-cogs"></i> Manage User (for adminstrator)</a></li>
                                        </ul>
                                    </div>
                                    <figure>
                                        <img class="img-rounded img-responsive" src="https://avatarmaker.net/avatars_upload/s033030202.jpeg" alt="Invalidname's avatar">
                                        <figcaption class="text-center"><span style="font-style: italic !important"> Unwanted </span><br><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                            <i
                                                class="fa fa-star"></i><i class="fa fa-star-half"></i> </figcaption>
                                    </figure>
                                    <dl class="dl-horizontal">
                                            <dt>joined date:</dt>
                                            <dd>15 September 2014</dd>
                                            <dt>Posts:</dt>
                                            <dd>982</dd>
                                            <dt>Score:</dt>
                                            <dd>+89</dd>
                                            <dt>Upvotes:</dt>
                                            <dd>3.9k</dd>                  
                                    </dl>
                                </section>
                            </section>

                        </section>
                        <div class="panel-footer">
                            <div class="row">
                                <section class="col-md-2 ">
                                    <i class="fa fa-thumbs-up "></i><a href="#"> Thanks </a>| <i class="fa fa-warning "></i>
                                    <a
                                        href="#"> Report </a>
                                </section>
                                <section id="thanks" class="col-md-6">
                                    <small><a href="#" data-toggle="tooltip" title="You and david,ehsan,john doe">  who L<i class="fa fa-heart "></i>ve this!</a> </small><br>

                                </section>
                                <section class="col-md-4">
                                    <span class="fa-stack">
                              <i class="fa fa-quote-right fa-stack-1x"></i>
                              <i class="fa fa-comment-o fa-lg fa-stack-1x"></i>
                          </span><a href="#"> Reply With Quote </a> |
                                    <i class="fa fa-mail-reply "></i><a href="#"> Reply </a>|
                                    <i class="fa fa-edit "></i><a href="#"> Edit Post </a>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 column">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <section class="panel-title">
                                    <time class="pull-right">
                              <i class="fa fa-calendar"></i> 2006-09-15 , <i class="fa fa-clock-o"></i> 1:35 pm
                              </time>
                                    <section class="pull-left" id="id">
                                        <abbr title="count of posts in this topic">#1</abbr>
                                    </section>
                                </section>
                            </div>
                            <section class="row panel-body">
                                <section class="col-md-9">
                                   
                                    <hr> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque consectetur ultrices
                                    dapibus. Curabitur ultricies lobortis leo, in euismod odio tincidunt sed. Integer ultrices
                                    dolor quis velit pharetra volutpat. Cras pharetra quis purus vitae volutpat. Curabitur pharetra
                                    ex ut libero venenatis malesuada.  <blockquote>
                                        Mauris placerat dolor nunc, eu viverra nibh eleifend mollis.
                                    Maecenas egestas blandit odio, a accumsan risus scelerisque ac. Suspendisse pharetra imperdiet
                                    purus nec sodales. Aliquam eget metus gravida, imperdiet nisi eget, porta velit. Etiam eu
                                    efficitur leo. Aliquam erat volutpat. Sed urna metus, fermentum vitae mauris in, placerat
                                    consectetur urna.
                                    </blockquote>  Cras ornare dignissim mi. Praesent aliquam aliquet arcu a viverra. In id
                                    sagittis ligula. Nullam ut diam tellus.
                                </section>
    
                                <section id="user-description" class="col-md-3 ">
                                    <section class="well">
                                        <div class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cricle"></i>Invalidname<span class="caret"></span></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#"><i class="fa fa-user"></i> See profile</a></li>
                                                <li><a href="#"><i class="fa fa-envelope"></i> Send PM</a></li>
                                                <li><a href="#"><i class="fa fa-code"></i>View all Articles</a></li>
                                                <li><a href="#"><i class="fa fa-th-list"></i>View all Posts</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="fa fa-plus"></i>Add to contact List</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="fa fa-cogs"></i> Manage User (for adminstrator)</a></li>
                                            </ul>
                                        </div>
                                        <figure>
                                            <img class="img-rounded img-responsive" src="https://avatarmaker.net/avatars_upload/s033030202.jpeg" alt="Invalidname's avatar">
                                            <figcaption class="text-center"> <span style="font-style: italic !important"> Unwanted </span> </i> <br><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                                <i
                                                    class="fa fa-star"></i><i class="fa fa-star-half"></i> </figcaption>
                                        </figure>
                                        <dl class="dl-horizontal">
                                                <dt>joined date:</dt>
                                            <dd>15 September 2014</dd>
                                            <dt>Posts:</dt>
                                            <dd>982</dd>
                                            <dt>Score:</dt>
                                            <dd>+89</dd>
                                            <dt>Upvotes:</dt>
                                            <dd>3.9k</dd>               
                                        </dl>
                                    </section>
                                </section>
    
                            </section>
                            <div class="panel-footer">
                                <div class="row">
                                    <section class="col-md-2 ">
                                        <i class="fa fa-thumbs-up "></i><a href="#"> Thanks </a>| <i class="fa fa-warning "></i>
                                        <a
                                            href="#"> Report </a>
                                    </section>
                                    <section id="thanks" class="col-md-6">
                                        <small><a href="#" data-toggle="tooltip" title="You and david,ehsan,john doe">  who L<i class="fa fa-heart "></i>ve this!</a> </small><br>
    
                                    </section>
                                    <section class="col-md-4">
                                        <span class="fa-stack">
                                  <i class="fa fa-quote-right fa-stack-1x"></i>
                                  <i class="fa fa-comment-o fa-lg fa-stack-1x"></i>
                              </span><a href="#"> Reply With Quote </a> |
                                        <i class="fa fa-mail-reply "></i><a href="#"> Reply </a>|
                                        <i class="fa fa-edit "></i><a href="#"> Edit Post </a>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
    
            </div>



        </section>
    </section>
</section>
@endsection
 
@section('xjs')
@endsection