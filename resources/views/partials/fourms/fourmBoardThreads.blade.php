<div class="section">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Mabi</h3>
                </div>
    
                <div class="panel-body">
    
                    @forelse($boardThreads as $thread)
                    <div class="row">
                        <div class="col-md-9">
                            <div class="list-group">
    
    
    
    
    
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><a href=" {{route('singlethread.show',['board' => $fourmBoard->slug, 'thread' => $thread->slug])}}"> {{$thread->title}}</a></h3>
                                    </div>
                                    <div class="panel-body">
                                        <p>
                                            {{$thread->subject_text}}
                                        </p>
                                    </div>
                                </div>
    
    
    
    
                            </div>
                        </div>
                        <div class="col-md-3">
                                <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><a href=" {{route('singlethread.show',['board' => $fourmBoard->slug, 'thread' => $thread->slug])}}">Board Information</a></h3>
                                        </div>
                                        <div class="panel-body">
                                            <p>
                                                {{$thread->subject_text}}
                                            </p>
                                        </div>
                                    </div>
                        </div>
                    </div>
                    @empty
                    <h5>No threads</h5>
                    @endforelse
                </div>
    
    
    
    
    
    
    
            </div>
    
    
          
            
        </div>
    </div>