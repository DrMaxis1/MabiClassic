@extends('layouts.ui')
@section('canonical-url')
{{URL::current()}}
@endsection
@section('xcss')
<style>

</style>

@endsection

@section('content')


@include('fourms.partials.threadHeader')


@include('fourms.partials.threadCategories')


@include('fourms.partials.threadList')


@endsection


@section('xjs')

@endsection
