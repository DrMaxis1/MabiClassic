@extends('layouts.ui') 
@section('canonical-url') {{URL::current()}}
@endsection
 
@section('xcss')
<style>
</style>
@endsection
 
@section('content')


<h4>{{$thread->subject}}</h4>
<hr>

<div class="thread-details">
    {!! \Michelf\Markdown::defaultTransform($thread->thread) !!}
</div>
<br> @if(auth()->user()->id == $thread->user_id)
<div class="actions">

    <a href=" {{route('fourms.edit',$thread->id)}} " class="btn btn-info btn-xs">Edit</a> {{--//delete form--}}
    <form action="{{route('fourms.destroy',$thread->id)}}" method="POST" class="inline-it">
        {{csrf_field()}} {{method_field('DELETE')}}
        <input class="btn btn-xs btn-danger" type="submit" value="Delete">
    </form>

</div>
@endif
{{-- Thread Comments --}}
@foreach($thread->comments as $comment)
<div class="thread-comments">

    


    <p>{{$comment->body}}</p>
    <h4>{{$comment->user->name}}</h4>

    {{-- Thread Actions --}}
    <div class="actions">



        <a class="btn btn-primary btn-xs" data-toggle="modal" href='#{{$comment->id}}'>Edit</a>
        <div class="modal fade" id="{{$comment->id}}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        <div class="thread-comment-form">

                            <form action="{{route('comment.update',$comment->id)}}" method="post" role="form">
                                @csrf {{method_field('PUT')}}

                                <h3>Edit A Comment</h3>

                                <input type="text" class="form-control" name="comment_body" placeholder="Add a comment" value="{{$comment->body}}">


                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>

        {{-- <a href=" {{route('fourms.edit',$thread->id)}} " class="btn btn-info btn-xs">Edit</a> --}} {{--//delete form--}}
        <form action="{{route('comment.destroy',$comment->id)}}" method="POST" class="inline-it">
            @csrf {{method_field('DELETE')}}
            <input class="btn btn-xs btn-danger" type="submit" value="Delete">
        </form>

    </div>



{{-- Thread Comment Replies --}}
@foreach($comment->comments as $commentReply)

<div class="small well text-info replies">
    <p>{{$commentReply->body}}</p>
    <h4>{{$commentReply->user->name}}</h4>

<div class="actions">



    <a class="btn btn-primary btn-xs" data-toggle="modal" href='#{{$commentReply->id}}'>Edit</a>
    <div class="modal fade" id="{{$commentReply->id}}">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <div class="modal-body">
                    <div class="thread-comment-form">

                        <form action="{{route('comment.update',$commentReply->id)}}" method="post" role="form">
                            @csrf {{method_field('PUT')}}

                            <h3>Edit A Comment</h3>
                            <div class="form-group">
                                <input type="text" class="form-control" name="comment_body" placeholder="Add a comment" value="{{$commentReply->body}}">
                            </div>
                            


                            <button type="submit" class="btn btn-primary">Reply</button>
                        </form>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    {{-- <a href=" {{route('fourms.edit',$thread->id)}} " class="btn btn-info btn-xs">Edit</a> --}} 
    
    {{--//delete form--}}
    <form action="{{route('comment.destroy',$comment->id)}}" method="POST" class="inline-it">
        @csrf {{method_field('DELETE')}}
        <input class="btn btn-xs btn-danger" type="submit" value="Delete">
    </form>

</div>


{{-- Thread Comment Reply Form  --}}



</div>
@endforeach
    
<div class="thread-comment-reply-form">

    <form action="{{route('commentreply.store',$comment->id)}}" method="post" role="form">
        @csrf

        <h3>Reply To this comment</h3>
<div class="form-group">
    <input type="text" class="form-control" name="comment_body" placeholder="Add a comment">
</div>
        


        <button type="submit" class="btn btn-primary">Post</button>
    </form>
</div>







</div>
@endforeach
{{-- Thread Form --}}

<div class="thread-comment-form">

    <form action="{{route('threadcomment.store',$thread->id)}}" method="post" role="form">
        @csrf

        <h3>Add A Comment</h3>
<div class="form-grou">
    <input type="text" class="form-control" name="comment_body" placeholder="Add a comment">
</div>
        


        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection
 
@section('xjs')
@endsection