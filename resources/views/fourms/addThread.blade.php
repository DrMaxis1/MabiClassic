@extends('layouts.ui')
@section('canonical-url')
{{URL::current()}}
@endsection
@section('xcss')
<style>

</style>

@endsection

@section('content')



<div class="row" style="justify-content: center;
display: flex;">
    <div class="col-md-6">
        <div class=" well">
            <form class="form-vertical" action="{{route('fourms.store')}}" method="post" role="form"
                  id="create-thread-form">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="subject">Subject</label>
                    <input type="text" class="form-control" name="subject" id="" placeholder="Input..."
                           value="{{old('subject')}}">
                </div>

                <div class="form-group">
                    <label for="type">Type</label>
                    <input type="text" class="form-control" name="type" id="" placeholder="Input..."
                           value="{{old('type')}}">
                </div>

                <div class="form-group">
                    <label for="thread">Thread</label>
                    <textarea class="form-control" name="thread" id="" placeholder="Input..."
                    > {{old('thread')}}</textarea>
                </div>

                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="form-group col-md-4">
                        <div class="captcha">
                           <span class="refereshrecapcha">{!! captcha_img('flat') !!}</span>
                            <button type="button" class="btn btn-success"><i class="fa fa-refresh" href="javascript:void(0)" onclick="refreshCaptcha()"></i></button>
                        </div>
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="form-group col-md-4">
                        <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha"></div>
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
        
    </div>


@endsection


@section('xjs')
<script src="{{asset('js/app.js')}}"></script>
    <script>
    
    function refreshCaptcha(){
    $.ajax({
    url: "/refreshcaptcha",
    type: 'get',
      dataType: 'html',        
      success: function(json) {
        $('.refereshrecapcha').html(json);
      },
      error: function(data) {
        alert('Try Again.');
        console.log(data);
      }
    });
    }
    
    
    </script>
@endsection
