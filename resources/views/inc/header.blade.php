<div class="container">
    <div class="breadcrumb-container" style="height: 150;
            background: rgba(0, 0, 0, 0) url(https://drmaxis.github.io/BellsFerry/img/products-banner/blue-wicker.png) no-repeat scroll center center / cover;">
        <div class="row text-center" style="justify-content: center;/* align-items: center; */display: flex;">
            <!--/-/-/-/-/-/-/-/-/
                            Begin Logo 
                        -/-/-/-/-/-/-/-/-/-->

            <div class="header-logo col-xs-2 text-center">
                <a href="/">
                <img src="https://www.droidgamers.com/wp-content/uploads/2017/07/mabinogi-android-1024x670.png"  style="max-width: 150px;" alt="brand-image">
                        </a>

                <!--/-/-/-/-/-/-/-/-/
                            Begin Breadcrumb Title
                        -/-/-/-/-/-/-/-/-/-->

                <h1 style="
                        margin: 0px;
                        font-size: 14px;
                        text-transform: uppercase;
                    "><span style="display:none;">MabiPro: The </span>Authentic&nbsp;Old-School Mabinogi&nbsp;Experience</h1>
                <!--/-/-/-/-/-/-/-/-/
                            End Breadcrumb Title
                        -/-/-/-/-/-/-/-/-/-->

            </div>


            <!--/-/-/-/-/-/-/-/-/ 
                        End Logo
                        -/-/-/-/-/-/-/-/-/-->




            <!--/-/-/-/-/-/-/-/-/
                            Begin Breadcrumb Login/Register 
                        -/-/-/-/-/-/-/-/-/-->
            {{--
            <div class="auth-links">
                <nav>
                    <ul class="primary-menu-list text-center list-inline ">


                    </ul>

                </nav>

            </div>
            --}}
            <!--/-/-/-/-/-/-/-/-/
                            End Breadcrumb Title
                        -/-/-/-/-/-/-/-/-/-->

        </div>

    </div>


    <!--/-/-/-/-/-/-/-/-/ 
                       Begin Desktop Menu  
                        -/-/-/-/-/-/-/-/-/-->
    <div class="desktop-nav">
        <nav class="navbar">
            <ul class="primary-menu-list text-center list-inline ">
                <li class="list-inline-item"><a id="fadeLink" href="/">News</a></li>
                <li class="list-inline-item"><a id="fadeLink" href="/fourms">Fourms</a></li>
                <li class="list-inline-item"><a id="fadeLink" href="/">Events</a></li>
                <li class="list-inline-item"><a id="fadeLink" href="/">Support</a></li>
                @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
                @endif @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
                @endguest

            </ul>

        </nav>
    </div>


    <!--/-/-/-/-/-/-/-/-/ 
                                                    End Desktop Menu 
                                                    -/-/-/-/-/-/-/-/-/-->
</div>