<!--
  @MabiClassicDevTeam 2019
  MabiClassic.com
  @StudioUnwanted 2019
  StudioUnwanted.com
  
-->

<html class="no-js" lang="{{ app()->getLocale() }}">
    @include('inc.head') @yield('xcss')

<body>


    <section class="xenx-box">


        <!--/-/-/-/-/-/-/-/-/
    Begin UI-Container  
-/-/-/-/-/-/-/-/-/-->
        {{--
        <div class="wrapper"> --}}

            <section>
    @include('inc.header')


                <section class="xeny-box stretch">

                    <section id="content">
                        <section class="xeny-box stretch">
                            <section>
                                <section class="xenx-box">
                                    <section id="main-sec" class="scrollable padder-lg w-f-md">

                                        <div class="container" style="width:100%;">
                                            <div class="row row-sm">
                                                    @include('inc.notifications')
                                                @yield('content')

                                            </div>
                                        </div>



                                        <!--/-/-/-/-/-/-/-/-/
    End UI-Container  
-/-/-/-/-/-/-/-/-/-->














                                        <!--/-/-/-/-/-/-/-/-/
    Begin JAVASCRIPT  
-/-/-/-/-/-/-/-/-/-->












                                        <!--/-/-/-/-/-/-/-/-/
    END JAVASCRIPT  
-/-/-/-/-/-/-/-/-/-->


                                    </section>
                                </section>
                            </section>
                        </section>
                    </section>
                </section>
            </section>

            <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
            


            <!--/-/-/-/-/-/-/-/-/ 
    Mobile Menu  
-/-/-/-/-/-/-/-/-/-->
            <script type="text/javascript" src="{{asset('js/jquery.meanmenu.min.js')}}"></script>


             <!--/-/-/-/-/-/-/-/-/ 
    Scroll to Top
-/-/-/-/-/-/-/-/-/-->
<script type="text/javascript" src="{{asset('js/jquery.scrollUp.js')}}"></script>

            <!--/-/-/-/-/-/-/-/-/ 
       MabiClassic
      -/-/-/-/-/-/-/-/-/-->
            <script src="{{asset('js/mabiclassic.js')}}"></script>

            @yield('xjs')



</body>

</html>