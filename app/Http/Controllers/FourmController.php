<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FourmBoard;

class FourmController extends Controller
{
    

 

   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fourmBoards = FourmBoard::paginate(15);

        return view('fourms', compact('fourmBoards'));
    }




     /**
     * Display the specified resource.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $fourmBoard = FourmBoard::where('slug', '=', $slug)->get()->first();
        $boardThreads = $fourmBoard->threads()->get();

        return view('singleFourmBoard')->with([
            'fourmBoard' => $fourmBoard,
            'boardThreads' =>$boardThreads,
        ]);
    }



       /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fourmboard.create');
    }


     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate
        $this->validate($request, [
            'subject' => 'required|min:5',
            'description'  => 'required|min:10',
            'captcha' => 'required|captcha',
        ]);
        //store

        /* $thread = */
        auth()->user()->fourmBoards()->create($request->all());
        /* $thread->tags()->attach($request->tags);  */
        //redirect

        return redirect()->back()->with('success_message', 'Thread Created!');
    }


 /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function edit(FourmBoard $board)
    {
        $fourmBoard = FourmBoard::where('slug', '=', $board->slug)->get()->first();
        return view('fourms.editBoard')->with([
            'fourmBoard' => $fourmBoard,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FourmBoard $board)
    {
        if (auth()->user()->id !== $board->user_id) {
            abort(401, "unauthorized");
        }


        //validate 
        $this->validate($request, [
            'subject' => 'required|min:5',
            'description'  => 'required|min:10',
            'captcha' => 'required|captcha',
        ]);
        $board->update($request->all());
        return redirect()->route('fourms.show', $board->slug)->with('success_message', 'Board Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function destroy(FourmBoard $board)
    {


        $board = FourmBoard::where('slug', '=', $board->slug)->get()->first();

        if (auth()->user()->id !== $board->user_id) {
            abort(401, "unauthorized");
        }


        $board->delete();
        return redirect()->route('fourms.index')->with('success_message', 'Board Deleted!');
    }


}
