<?php

namespace App\Http\Controllers;

use App\Thread;
use App\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{



    public function addThreadComment(Request $request, Thread $thread)
    {
        $this->validate($request, [
            'comment_body' => 'required',
        ]);
        $comment = new Comment();
        $comment->body = $request->comment_body;
        $comment->user_id = auth()->user()->id;
        $thread->comments()->save($comment);

        return redirect()->back()->with('success_message', 'Comment has been added!');
        
    }

    public function addCommentReply(Request $request, Comment $comment)
    {
        $this->validate($request, [
            'comment_body' => 'required',
        ]);
        $commentReply = new Comment();
        $commentReply->body = $request->comment_body;
        $commentReply->user_id = auth()->user()->id;


        $comment->comments()->save($commentReply);

        return redirect()->back()->with('success_message', 'Comment has been added!');
        
    }
  

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $comment = Comment::where('id','=',$id);
        if($comment->user_id !== auth()->user()->id) 
        return back()->withErrores('You are not authorized to do this! ');


        $this->validate($request, [
            'comment_body' => 'required',
        ]);



        $comment->update($request->comment_body);

        return redirect()->back()->with('success_message', 'Comment Updated');
    }




    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        if($comment->user_id !== auth()->user()->id) 
        return back()->withErrores('You are not authorized to do this! ');

        $comment->delete();
        
        return redirect()->back()->with('success_message', 'Comment Deleted');
        
    }
}