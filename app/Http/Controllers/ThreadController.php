<?php

namespace App\Http\Controllers;

use App\Thread;
use App\FourmBoard;
use App\FourmThread;
use Illuminate\Http\Request;

class ThreadController extends Controller
{




   


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $threads = Thread::paginate(15);

        return view('fourms.threadHome', compact('threads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fourms.addThread');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate
        $this->validate($request, [
            'subject' => 'required|min:5',
            'tags'    => 'max:200',
            'thread'  => 'required|min:10',
            'captcha' => 'required|captcha',
        ]);
        //store

        /* $thread = */
        auth()->user()->threads()->create($request->all());
        /* $thread->tags()->attach($request->tags);  */
        //redirect

        return redirect()->back()->with('success_message', 'Thread Created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function show($board, $thread)
    {
        $board = FourmBoard::where('slug', '=', $board)->get()->first();
        $thread = FourmThread::where('slug', '=', $thread)->get()->first();

       
        return view('partials.fourms.singleFourmBoardThread')->with([
            'thread' => $thread,
            'board' => $board,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $thread = Thread::where('id', '=', $id)->get()->first();
        return view('fourms.editThread')->with([
            'thread' => $thread,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Thread $thread)
    {
        if (auth()->user()->id !== $thread->user_id) {
            abort(401, "unauthorized");
        }


        //validate 
        $this->validate($request, [
            'subject' => 'required|min:10',
            'type'    => 'min:20',
            'thread'  => 'required|min:20'
        ]);
        $thread->update($request->all());
        return redirect()->route('fourms.show', $thread->id)->with('success_message', 'Thread Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


        $thread = Thread::where('id', '=', $id)->get()->first();

        if (auth()->user()->id !== $thread->user_id) {
            abort(401, "unauthorized");
        }


        $thread->delete();
        return redirect()->route('fourms.index')->with('success_message', 'Thread Deleted!');
    }
}
