<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FourmThread extends Model
{
    protected $table = 'fourm_threads';
    protected $fillable = ['title','slug', 'subject_text','meta_info','type'];
    protected $guarded = [];



    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function fourmBoard()
    {
        return $this->belongsTo('App\FourmBoard');
    }

    public function fourmPosts()
    {
        return $this->morphMany('App\FourmPost', 'fourmpoststable');
    }
}
