<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FourmPost extends Model
{

    protected $fillable = ['body', 'user_id'];
    
    /**
     * 
     *  Get all of the owning commenttable models
     * 
     */

     public function fourmpoststable() {
         return $this->morphTo();
     }

     public function user() {
        return $this->belongsTo('App\User');
    }

    public function fourmPosts() {
        return $this->morphMany('App\FourmPost', 'fourmpoststable');
    }
 
}
