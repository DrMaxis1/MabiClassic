<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FourmBoard extends Model
{
    protected $table = 'fourm_boards';
    protected $fillable = ['subject','description','slug','meta_info'];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function threads() {
        return $this->hasMany('App\FourmThread');
    }
}
