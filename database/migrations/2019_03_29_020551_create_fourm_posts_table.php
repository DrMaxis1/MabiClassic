<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFourmPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fourm_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('body');
            $table->string('fourmpoststable_id');
            $table->string('fourmpoststable_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fourm_posts');
    }
}
