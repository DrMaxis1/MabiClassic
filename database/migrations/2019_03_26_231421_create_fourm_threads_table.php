<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFourmThreadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fourm_threads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('fourm_board_id');
            $table->longText('title');
            $table->string('slug')->nullable();
            $table->longText('subject_text');
            $table->string('meta_info')->nullable();
            $table->string('type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fourm_threads');
    }
}
