<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

 Route::get('/refreshcaptcha', 'CaptchaController@refreshCaptcha'); 
/* Route::get('/refreshcaptcha', function () {
    return response()->json(['captcha'=> captcha_img()]);
}); */
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');






/*
|--------------------------------------------------------------------------
| Fourm Routes
|--------------------------------------------------------------------------
|
| Here is where we register Fourm routes for this application. 
|
*/





Route::get('/fourms', 'FourmController@index')->name('fourms.index');

Route::get('/fourms/create', 'FourmController@create')->name('fourms.create');

Route::post('/fourms/store', 'FourmController@store')->name('fourms.store');

Route::get('/fourms/{fourm}', 'FourmController@show')->name('fourms.show');

Route::get('/fourms/edit/{fourm}', 'FourmController@edit')->name('fourms.edit');

Route::delete('/fourms/destroy{fourm}', 'FourmController@destroy')->name('fourms.destroy');

Route::put('/fourms/update/{thread}', 'FourmController@update')->name('fourms.update');



/*
|--------------------------------------------------------------------------
| Fourm Thread Routes
|--------------------------------------------------------------------------
|
| Here is where we register Fourm Thread routes for this application. 
|
*/

Route::get('/fourms/{board}/{thread}', 'ThreadController@show')->name('singlethread.show');



/*
|--------------------------------------------------------------------------
| Fourm Commenting 
|--------------------------------------------------------------------------
|
| Here is where we register Fourm routes for this application. 
|
*/

Route::resource('comment', 'CommentController', ['only' => ['update','destroy']]);

Route::post('comment/create/{thread}', 'CommentController@addThreadComment')->name('threadcomment.store');

Route::post('reply/{comment}', 'CommentController@addCommentReply')->name('commentreply.store');

Route::get('/test', function () {


});


