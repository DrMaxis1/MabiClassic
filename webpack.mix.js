const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .js('resources/js/mabiclassic.js', 'public/js')
   .js('resources/js/jquery.meanmenu.min.js', 'public/js')
   .js('resources/js/jquery.scrollUp.js', 'public/js')
   .sass('resources/sass/meanmenu.min.scss', 'public/css')
   .sass('resources/sass/bootstrap.min.scss', 'public/css')
   .sass('resources/sass/mabiclassic.scss', 'public/css');
